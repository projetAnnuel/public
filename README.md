Generer clé SSH
$ cd ~/.ssh
$ ls
Checker si id_rsa et id_rsa.pub sont présent, si ils ne le sont pas :

$ ssh-keygen -t -rsa

Lorsque la key est générée :

$ cat ~/.ssh/id_rsa.pub
Copiez l'intégralité et mettez la dans l'interface git
